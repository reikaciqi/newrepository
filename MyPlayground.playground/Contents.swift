
import UIKit

//deklarim variabli i tipit karakter

var karakteri: Character = "A"
// deklarim i nje variabli i cili mban nje varg karakteresh
var str = "Hello, playground"
var str2 = String("Hi")
//nese nje string eshte bosh
if str2.isEmpty{
    print("str2 eshte bosh")
}
//printim ne ekran i vleres se variablit
print(str)

//variabel qe mban nje vlere te  tipit int
var nr=5
//variable qe mban nje vlere float
var floatvalue=3.12

//variabel qe mban nje vlere double
var doublevalue=3.14159

print("vlera e variablit floatvalue eshte : \(floatvalue)")
//variabel boolean i cili mban nje true apo false
var trueBool=true
var falseBool=false

//variabel konstant qe perdoret ne rast se nuk duam te nderrojme vleren e tij
let konstante="variabel konstant"

//variabel i percajktuar si opsional dhe nil
var myString: String? = nil

//struktura if else per te pare nese variabli eshte i ndryshem nga nil
if myString != nil {
    
    print(myString!)
} else {
   print("myString ka vleren nil")
}

//operatoret aritmetike

var nr1=10
var nr2=5

var shuma=nr1+nr2
var zbritje=nr1-nr2
var shumezim=nr1*nr2
var pjesetim=nr1/nr2
var modul=nr1%nr2

//operatoret krahasimore
//nese jane te barabarte
if nr1==nr2 {
    print("te barabarta")
}
else{
    print("jo e vertete")
    
}

//nese jane te ndryshem

if nr1 != nr2 {
    print("te ndryshme")
}
else{
    print("jo e vertete")
    
}

//nese nr1 ehste me e madhe se nr2
if nr1>nr2 {
    print("\(nr1) > \(nr2)")
}
else{
    print("jo e vertete")
    
}

// nese nr1 eshte me e vogel se nr2
if nr1<nr2 {
    print("\(nr1) < \(nr2)")
}
else{
    print("jo e vertete")
    
}

//nese nr1 ehste me e madhe ose baraz se nr2
if nr1>=nr2 {
    print("\(nr1) >= \(nr2)")
}
else{
    print("jo e vertete")
    
}

//nese nr1 ehste me e vogel ose baraz se nr2
if nr1<=nr2 {
    print("\(nr1) <= \(nr2)")
}
else{
    print("jo e vertete")
    
}


//guard
func ndryshoStringUpperCase(){
    //krijohet variabli opsional dhe i jepet vlere boshe ""
    let fjalia: String?=""
    //kontrollojme nese eshte nil ose bosh
    guard let temp = fjalia, temp.count>0 else {
        //nese eshte nil ose bosh , return
        print("variabli eshte nil ose nje string bosh")
        return
    }
    print("UpperCased : \(temp.uppercased())")
}





//operatoret logjike

if (nr1==10 && nr2==5) || (nr1>10 && nr2<5){
    //simboli && kerkobn qe te permbushen kushtet ne te dyja nanet e tij
    //simboli || kerkon qe te permbushte te pakten njeri nga kushtet ne dy anet e tij
}

//funksionet

//funksion pa parametra i cili kthen nje vlere te tipit string
func funksion1()->String{
    let emri="emri"
    return emri
}

//funksion me parametra qe kthen nje vlere int

func funksion2(numri1:Int, numri2:Int)->Int{
    return numri1*numri2
}

var thirrje=funksion2(numri1: nr1, numri2: nr2)

//tabelat array
//krijim i n je tabele bosh
var bosh=[String]()
//shtim ne tabele
bosh.append("hello")
bosh.append("hi")

//array i tipit int me 3 vlera
var intArray:[Int]=[10,20,30]
var indeksi=1
var vleraETabeles=intArray[indeksi]

//modifikim i vleres se dyte
intArray[1]=6

//cikli for-in
for item in intArray{
    print (item)
}

//perdorim i funksionit enumerated per kthimin e indeksit sebashu me vleren

for(index, item) in intArray.enumerated(){
    print("vlera ne indeksin \(index) eshte \(item)")
}

//bashkim i dy array

var tabA=[Int](repeating: 1, count:2)
var tabB=[Int](repeating: 0,count :3)
var tabC=tabA+tabB

//numri i elementeve te nej array
print(tabC.count)


//cikli while

var whileIndex=10

while whileIndex>0{
    print(whileIndex)
    whileIndex=whileIndex-1
}

//cikli repeat
//ne kete rast kushti vendoset ne fund
var repeatIndex=10
repeat{
    print(repeatIndex)
    repeatIndex=repeatIndex-1
}
while repeatIndex>0

//dictionary (fjalor)

var dictNum :[Int:String]=[1:"Nje", 2:"Dy", 3:" Tre"]

//krijimi i nje fjalori duke perdorur array

var qytetet=["Tirane", "Durres", "Vlore"]
var distanca=[2000, 3000, 1500]

let distancaQytet = Dictionary(uniqueKeysWithValues: zip(qytetet,distanca))

//closures, ruajtja e nje blloku kodi ne nje variabel
let pjestim = {
    (vlera1:Int, vlera2:Int)->Int in
    return vlera1/vlera2
}
let rezultatClosure = pjestim(200,10)

let ditelindja = {
    print("Happy birthday!")
}

ditelindja()


//closure me parameter
let ditelindje2 :(String) -> () = {
    name in print("Happy birthday \(name)")
}

ditelindje2("Emer")



//enumeracioni

enum Qytet:String {
    case Tirana="Mes"
    case Shkodra="Veri"
    case Korca="Jug"
}
//printojme vleren e case
print(Qytet.Shkodra.rawValue)


enum switchStatus{
    case on
    case off
}
//ndertojme funksionin per te nderruar statusin nqs eshte i kunderti i parametrit te derguar
func nderroStatusin(statusi:switchStatus)-> switchStatus{
    if statusi == .off{
        return .on
    }else{
        return .off
    }
}
var statusi: switchStatus = .off
//therrasim funksionin, nqs statusi ka qene on , do behet off
nderroStatusin(statusi: statusi)

//krijimi i nje strukture

struct person{
    
    private(set) public var emri:String
    private(set) public  var mbiemri:String
    private(set) public var mosha: Int


//krijojme funksionin iniciues te struktures
init(emri:String,mbiemri: String, mosha:Int){
    self.emri=emri
    self.mbiemri=mbiemri
    self.mosha=mosha
    
}
}
//krijojme nje array te tipit person

private let personat=[
    
    person(emri: "emri1", mbiemri: "mbiemri1", mosha: 20),
    person(emri: "emri2", mbiemri: "mbiemri2", mosha: 21),
    person(emri: "emri3", mbiemri: "mbiemri3", mosha: 30)
]
// ndertojme funksionin i cili na kthen array

func merrPersonat()->[person]{
return personat
}




//krijimi i klases

class Student{
    var emri: String
    var mbiemri: String
    var viti: Int
    var mesatarja: Float
    
    init(emri:String, mbiemri:String, viti:Int, mesatare: Float){ //konstruktori
        self.emri=emri
        self.mbiemri=mbiemri
        self.viti=viti
        self.mesatarja=mesatare
    }
    
    func merrEmrin()->String{
        return emri
    }
}
let student=Student(emri: "emer1", mbiemri: "mbiemri1", viti: 3, mesatare: 8.5)

print("Studenti me emrin \( student.emri), ne vitin \( student.viti) ka mesataren \( student.mesatarja)")


//tashegimie e klasave

class Person1{
   
    
    var emri: String
    var mbiemri: String
    var mosha: Int
    
    init( emri:String, mbiemri:String, mosha:Int) {
        self.emri=emri
        self.mbiemri=mbiemri
        self.mosha=mosha
    }
    func ditelindje(){
        self.mosha += 1
        print("mosha u rrit me 1,mosha juaj \(self.mosha)")
    }
}
// trashegojme nga klasa Person1
class Profesioni: Person1{
   var profesioni: String
   var viteEksperience: Int
    
    init(profesion: String, eksperienca : Int, emri:String, mbiemri:String, mosha: Int){
    self.profesioni=profesion
    self.viteEksperience=eksperienca
     super.init(emri: emri, mbiemri: mbiemri, mosha: mosha)
       }
 
}
//krijojme nje objekt te klases Profesioni dhe me ane te tij mund te aksesojme edhe metodat apo variablat e klases baze

var punonjes : Profesioni  = Profesioni(profesion: "IT", eksperienca: 2, emri:"emri", mbiemri:"mbiemri", mosha:20)

punonjes.ditelindje()
//Polimorfizmi

class Shape{
    var siperfaqe: Double?
    
    func llogaritSiperfaqen(A: Double, B : Double){
        self.siperfaqe=A * B
    }
}
//i njejti funksion per situata te ndryshme
class Trekendesh: Shape{
    override func llogaritSiperfaqen(A: Double, B: Double) {
        siperfaqe=(A*B)/2
    }
}

//protokoll

protocol flying {
    //funksioni brenda protokollin kthen nje string
    func fly()-> String
}

class Helicopter : flying{
    //shtojme funksionin e protokollit
    func fly() -> String {
        let fluturim="Helikopter"
        return fluturim
    }
}

//trashegimia e protokolleve

protocol driving: flying{
    var shpejtesia: Double {get}
}
// te gjithe variablat apo funksionet brenda protokolleve
class levizje : driving{
    var shpejtesia: Double
    
    func fly() -> String {
        return "helikopter"
    }
    init(shpejtesia: Double){
        self.shpejtesia=shpejtesia
    }
    
    
}



//extensions
// krijojme nje klase
class Aeroplan{
    var lartesia: Double = 0
    
    func jepLartesine(miles:Double){
        lartesia=miles
    }
}

//krijojme nje extension e cila na lejon qe te shtojme nje funksion qe jep lartesine ne kilometer
extension Aeroplan{
    func jepLartesine(km: Double){
        lartesia=km
    }
}

 let boeing = Aeroplan()
//funksioni i krijuar ne extension
boeing.jepLartesine(km: 10)


// extension klasa Int

extension Int{
    //vendosim mutating per te bere te mundur modifikimin me veten
    mutating func shumezimMeVeten(){
        self = self * self
    }
}

var numerInt=6
numerInt.shumezimMeVeten()

// extension i klases String

extension String {
    func reverse() -> String {
        var TabelaKaraktereve=[Character]()
        for karakter in self{
            TabelaKaraktereve.insert(karakter, at:0)
        }
        return String(TabelaKaraktereve)
    }
}

var strNew="Reverse"
var strnew1=strNew.reverse()
print(strnew1)
