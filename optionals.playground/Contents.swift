import UIKit
// nje forme regjistrimi kerkon emrin
let firstName : String = "firstname"
//emri middle eshte opsional
let middleName : String? = nil
let lastName: String = "lastname"


//verifikojme nese ka ndonje vlere brenda variablit opsional ose eshte nil
if let middle = middleName{
    
    print("\(firstName) \(middle) \(lastName)")
}else{
    print("\(firstName) \(lastName)")
}

func unwrapMeGuarg(){
    
    guard let middle = middleName else {
         print("\(firstName) \(lastName)")
        return
    }
    print("\(firstName) \(middle) \(lastName) ")
}


// closures

let printim = {
    print("hello")
}

printim()

//menyra 1
// kjo closure merr dy int si parameter dhe kthen nje int
let mbledhje1: (Int , Int)-> Int = { (nr1: Int, nr2: Int) -> Int in
    return nr1 + nr2
    
}

mbledhje1(1,2)

//menyra 2
let mbledhje2 = {
    (nr1: Int, nr2: Int)-> Int in
       nr1+nr2
}

mbledhje2(1,2)

//menyra3

let mbledhje3 :(Int, Int)-> Int = {
    $0 + $1
}

mbledhje3(1,2)

// marrja e nje closure si parameter ne funksion
func funksionMeClosure(theClosure: () -> Void ){
    theClosure()
}

// funksion i cili kthen nje closure

func ktheClosure() -> () ->Void {
    let cl = "This closure"
    return {
        print(cl)
    }
}

let closure = ktheClosure()
closure()


// renditja e tabelave

var emrat = ["emer1","emer2","emer3","emer4"]
emrat.sort()

//renditje mbi bazen e propertris

struct User{
    var emri: String
}

var users = [
    User(emri: "emri1"),
    User(emri: "emri2"),
    User(emri: "emri3")
]

users.sort {
    $0.emri < $1.emri
}


//nese duam te kthejme nje tabele te renditur

let tabelaerenditur = users.sorted {
    $0.emri < $1.emri
}


//emum dhe switch

enum Busulla {
    case veri
    case jug
    case lindje
    case perendim
    
}

let drejtimi = Busulla.jug

switch drejtimi {
case .veri:
    print("drejtimi ne veri")
    
case .jug:
    print("drejtimi ne jug")
    
case .lindje:
    print("drejtimi ne lindje")
case .perendim:
    print("drejtimi ne perendim")

}


// perdorimi i default ne rast se nuk perputhet asnje opsion

enum shtetet{
    case Italy
    case Greece
    case Germany
    case Albania

}


let origjina = shtetet.Albania

switch origjina {
case .Germany:
    print (" ")
case .Greece, .Italy:
    print(" ")
//nese nuk eshte asnje nga caset do ekzekutohet default
default:
    print("Albania")
}


//tuples
// jane variabla te cilat mund te mbajne vlera te ndryshme

let fluturim = (700, "TIA")

//mund te merret vlera e tyre me ane te indekseve
print(fluturim.0)
print(fluturim.1)

let fluturimi = (aeroplani: 747, aeroporti: "TIA")

print (fluturimi.aeroplani)
print(fluturimi.aeroporti)


for i in 1...100 {
    switch (i % 3 == 0, i % 5 == 0)
    {
    case (true, false):
        print ("true , false")
    case (false, true):
        print ("false, true")
    case (true, true):
        print("true , true")
    default:
        print(i)
    }
}

//nje cikel for i cili printon nga 1 ne 5

for index in 1...5 {
    print(index)
}

//cikel for qe printon tabelen

for vlera in [1,2,3,4,5] {
    print(vlera)
}

let vlerat = [2,7,11,4,6]

for index in 0..<vlerat.count {
    let vlera = vlerat[index]
    print(vlera)
}
 
var j = 0
while j < 10 {
    j+=1
}


var j2 = 0

repeat {
    j2 += 1
} while j2 < 10



let makina = (tipi: "benz", vendndodhja: "TR")

switch makina {
case ("benz", _):
    print("Marka e makines eshte benz")
case ( _, "TR"):
    print("makian ndodhet ne tirane")
    
default:
    print("tip dhe vendndodhje e panjohur e makines")
}





//protokollet

protocol Food {
    var kaloriPerGram: Double { get }
}

struct kafshet {
    //perdoret mutating per te bere te mundur mo0difikimin e variablit te brendshem
    var numerimKalorish: Double = 0
    mutating func ushqim (food: Food, gram: Double){
        numerimKalorish += food.kaloriPerGram * gram
    }
}

struct struktura: Food {
    var kaloriPerGram: Double {
        return 40
    }
    
    
}

let ushqimi = struktura()

var mace = kafshet()
mace.ushqim(food: ushqimi, gram: 30)


//generic functions
// ky lloj funksioni perdoret per tipe te ndryshme , por duhet qe te dy
//parametrat te jene te te njetit tip
func nderroDyVlera<T>(_ a: inout T, _ b: inout T) {
    let tmp = a
    a = b
    b = tmp
    
}
var numri1 : Int = 5
var numri2 : Int = 12

nderroDyVlera(&numri1, &numri2)

var fjala1 : String = "string"
var fjala2: String = "string2"

nderroDyVlera(&fjala1, &fjala2)


//krijimi i nje fjalori bosh

var fjalor = [String : String]()

var fjalor2: [String: String] = [
    "TIA": "Tirana International Airport",
    "PRN": "Prishtina International Airport"]

var aeroporti = fjalor2["TIA"]

print("vlera e pare eshte \(String(describing: aeroporti))")
print("vlera e dyte eshte \(String(describing: fjalor2["PRN"]))")


//ndrshimi i vleres se nje celesi

var someDict: [Int: String] = [1: "One", 2:"Two", 3:"Three"]

//ruajme ne nje variabel vleren e vjeter nderkohe qe e e modifikojme

var vleraVjeter = someDict.updateValue("Ndryshuar", forKey: 1)
var somevar = someDict[1]
print("Vlera e vjeter e celesit 1 eshte \(String(describing: vleraVjeter))")
print("Vlera e  re eshte \(String(describing: somevar))")


//heqja e nje vlere nga fjalori

var hequr = someDict.removeValue(forKey: 3)
someDict[3] = nil


